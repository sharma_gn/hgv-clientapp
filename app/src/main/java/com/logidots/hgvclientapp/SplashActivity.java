package com.logidots.hgvclientapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

public class SplashActivity extends AppCompatActivity {
    TextView splashText;
    Animation animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        animation = AnimationUtils.loadAnimation(SplashActivity.this,R.anim.anim_blink);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                loadPage();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        splashText.setAnimation(animation);
    }

    private void loadPage() {
        startActivity(new Intent(SplashActivity.this,LoginActivity.class));
        finish();
    }

    private void initViews() {
        splashText = findViewById(R.id.splashText);
    }
}
