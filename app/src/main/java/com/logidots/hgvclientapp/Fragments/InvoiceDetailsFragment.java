package com.logidots.hgvclientapp.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.logidots.hgvclientapp.R;


public class InvoiceDetailsFragment extends AppCompatDialogFragment {
    View view;
    AppCompatTextView invoiceDetailID,invoiceDetailPay,invoiceDetailReq,invoiceDetailPickupLoc,invoiceDetailDropLoc;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_invoice_details, container, false);
        initViews();
        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
            InsetDrawable inset = new InsetDrawable(back, 30);
            dialog.getWindow().setBackgroundDrawable(inset);
            //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            //dialog.setCancelable(false);
        }
    }

    private void initViews() {
        invoiceDetailID = view.findViewById(R.id.invoice_id_detail);
        invoiceDetailReq = view.findViewById(R.id.invoice_req_detail);
        invoiceDetailPay = view.findViewById(R.id.invoice_detail_pay);
        invoiceDetailPickupLoc = view.findViewById(R.id.invoice_detail_pick_loc);
        invoiceDetailDropLoc = view.findViewById(R.id.invoice_detail_drop_loc);
    }
}