package com.logidots.hgvclientapp.Fragments;

import android.os.Bundle;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.logidots.hgvclientapp.Adapters.RequestAdapter;
import com.logidots.hgvclientapp.CustomClass.RecyclerViewItemTouchListener;
import com.logidots.hgvclientapp.HomeActivity;
import com.logidots.hgvclientapp.Interfaces.ItemClickListener;
import com.logidots.hgvclientapp.R;


public class PendingRequestFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    //private OnFragmentInteractionListener mListener;
    View view;
    RecyclerView pendingReqRV;
    AppCompatTextView emptyPendingReq;
    RequestAdapter requestAdapter;

    public PendingRequestFragment() {
        // Required empty public constructor
    }

    public static PendingRequestFragment newInstance() {
        PendingRequestFragment fragment = new PendingRequestFragment();
        /*Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_pending_request, container, false);
        initViews();
        showData();
        return view;
    }

    private void showData() {
        requestAdapter = new RequestAdapter();
        pendingReqRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        pendingReqRV.setAdapter(requestAdapter);
        pendingReqRV.addOnItemTouchListener(new RecyclerViewItemTouchListener(getActivity(),
                pendingReqRV,new ItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.main_frame,DriverListFragment.newInstance());
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                transaction.addToBackStack(HomeActivity.class.getSimpleName());
                transaction.commit();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void initViews() {
        pendingReqRV = view.findViewById(R.id.pendingReqRV);
        emptyPendingReq = view.findViewById(R.id.emptyPendingReq);
    }
}


/*public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }*/