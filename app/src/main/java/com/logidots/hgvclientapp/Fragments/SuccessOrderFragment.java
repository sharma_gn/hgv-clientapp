package com.logidots.hgvclientapp.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.logidots.hgvclientapp.HomeActivity;
import com.logidots.hgvclientapp.R;

public class SuccessOrderFragment extends AppCompatDialogFragment {
    private View view;
    AppCompatButton orderSuccessButton;

    public SuccessOrderFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static SuccessOrderFragment newInstance(FragmentManager fragmentManager) {
        SuccessOrderFragment fragment = new SuccessOrderFragment();
        fragment.show(fragmentManager,"Order Successful");
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_success_order, container, false);
        orderSuccessButton = view.findViewById(R.id.btnOrderSuccess);
        orderSuccessButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), HomeActivity.class));
                getActivity().finish();
            }
        });
        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            ColorDrawable backgroundColor = new ColorDrawable(Color.TRANSPARENT);
            InsetDrawable insetDrawable = new InsetDrawable(backgroundColor,30);
            dialog.getWindow().setBackgroundDrawable(insetDrawable);
            dialog.setCancelable(false);
        }
    }
}
