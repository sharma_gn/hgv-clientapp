package com.logidots.hgvclientapp.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.logidots.hgvclientapp.R;

public class DriverDetailsFragment extends AppCompatDialogFragment implements View.OnClickListener {
    View view;
    AppCompatButton acceptButton,declineButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_driver_details, container, false);
        initViews();
        return view;
    }

    private void initViews() {
        acceptButton = view.findViewById(R.id.acceptBtn);
        declineButton = view.findViewById(R.id.declineBtn);

        acceptButton.setOnClickListener(this);
        declineButton.setOnClickListener(this);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null)
        {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
            ColorDrawable background = new ColorDrawable(Color.TRANSPARENT);
            InsetDrawable insetDrawable = new InsetDrawable(background,30);
            dialog.getWindow().setBackgroundDrawable(insetDrawable);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.acceptBtn:
                break;
            case R.id.declineBtn:
                dismiss();
                break;
            default:
                break;
        }
    }
}