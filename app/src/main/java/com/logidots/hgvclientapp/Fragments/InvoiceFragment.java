package com.logidots.hgvclientapp.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.logidots.hgvclientapp.Adapters.InvoiceAdapter;
import com.logidots.hgvclientapp.CustomClass.RecyclerViewItemTouchListener;
import com.logidots.hgvclientapp.Interfaces.ItemClickListener;
import com.logidots.hgvclientapp.R;


public class InvoiceFragment extends Fragment {
    /*private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;*/
    View view;
    RecyclerView invoiceRecyclerView;
    Toolbar toolbar;

    public InvoiceFragment() {
        // Required empty public constructor
    }

    public static InvoiceFragment newInstance() {
        InvoiceFragment fragment = new InvoiceFragment();
        /*Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_invoice, container, false);
        initViews();
        invoiceRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        invoiceRecyclerView.setAdapter(new InvoiceAdapter());
        invoiceRecyclerView.addOnItemTouchListener(new RecyclerViewItemTouchListener(getActivity(),
                invoiceRecyclerView, new ItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                DialogFragment invoiceDetails = new InvoiceDetailsFragment();
                invoiceDetails.show(getActivity().getSupportFragmentManager(),"Invoice Details");
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        return view;
    }

    private void initViews() {
        invoiceRecyclerView = view.findViewById(R.id.invoiceRV);
        toolbar = view.findViewById(R.id.toolbar);

        setupToolbar();
    }

    private void setupToolbar() {
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.left_arrow_64);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
    }
}

/*public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }*/