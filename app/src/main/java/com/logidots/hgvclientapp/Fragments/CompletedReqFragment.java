package com.logidots.hgvclientapp.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.logidots.hgvclientapp.Adapters.CompletedReqAdapter;
import com.logidots.hgvclientapp.R;

public class CompletedReqFragment extends Fragment {
    /*private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;*/
    View view;
    RecyclerView completedReqRV;
    AppCompatTextView emptyCompletedReq;

    public CompletedReqFragment() {
        // Required empty public constructor
    }


    public static CompletedReqFragment newInstance() {
        CompletedReqFragment fragment = new CompletedReqFragment();
        /*Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_completed_req, container, false);
        initViews();
        showData();
        return view;
    }

    private void showData() {
        completedReqRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        completedReqRV.setAdapter(new CompletedReqAdapter());
    }

    private void initViews() {
        completedReqRV = view.findViewById(R.id.completedReqRV);
        emptyCompletedReq = view.findViewById(R.id.emptyCompletedReq);
    }
}


/*public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }*/