package com.logidots.hgvclientapp.Fragments;


import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.logidots.hgvclientapp.R;


public class ProfileFragment extends Fragment implements View.OnClickListener {
    /*private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;*/

    View view;
    AppCompatEditText profileName,profileGender,profileNationality,profileAddress,profilePhone,profileEmail;
    AppCompatButton profileSave,profileEdit;
    FloatingActionButton editProfileImage;
    Toolbar toolbar;

    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        /*Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        initViews();
        return view;
    }

    private void initViews() {
        profileName = view.findViewById(R.id.profileNameText);
        profileGender = view.findViewById(R.id.profileGenderText);
        profileNationality = view.findViewById(R.id.profileNationalityText);
        profileAddress = view.findViewById(R.id.profileAddressText);
        profilePhone = view.findViewById(R.id.profilePhoneText);
        profileEmail = view.findViewById(R.id.profileEmailText);
        profileSave = view.findViewById(R.id.profileSaveBtn);
        profileEdit = view.findViewById(R.id.profileEditBtn);
        editProfileImage = view.findViewById(R.id.profileImageEdit);
        toolbar = view.findViewById(R.id.toolbar);

        profileSave.setOnClickListener(this);
        profileEdit.setOnClickListener(this);
        editProfileImage.setOnClickListener(this);

        setupToolbar();
    }

    private void setupToolbar() {
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.left_arrow_64);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.profileSaveBtn:
                profileName.setEnabled(false);
                profileGender.setEnabled(false);
                profileNationality.setEnabled(false);
                profileAddress.setEnabled(false);
                profilePhone.setEnabled(false);
                profileEmail.setEnabled(false);
                profileSave.setVisibility(View.GONE);
                profileEdit.setVisibility(View.VISIBLE);
                editProfileImage.setVisibility(View.GONE);
                Toast.makeText(getActivity(),R.string.update_successful,Toast.LENGTH_SHORT).show();
                break;
            case R.id.profileEditBtn:
                profileName.setEnabled(true);
                profileGender.setEnabled(true);
                profileNationality.setEnabled(true);
                profileAddress.setEnabled(true);
                profilePhone.setEnabled(true);
                profileEmail.setEnabled(true);
                profileSave.setVisibility(View.VISIBLE);
                profileEdit.setVisibility(View.GONE);
                editProfileImage.setVisibility(View.VISIBLE);
                break;
            case R.id.profileImageEdit:
                break;
            default:
                break;
        }
    }
}


/*public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }*/