package com.logidots.hgvclientapp.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioGroup;

import com.jaiselrahman.hintspinner.HintSpinner;
import com.jaiselrahman.hintspinner.HintSpinnerAdapter;
import com.logidots.hgvclientapp.Adapters.SpinnerAdapter;
import com.logidots.hgvclientapp.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class SubmitReqFragment extends Fragment implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    /*private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;*/
    View view;
    AppCompatTextView pickupDate,deliveryDate,pickupTime,deliveryTime;
    HintSpinner hgvCountSpinner,hgvTypeSpinner;
    AppCompatButton submitReqButton;
    RadioGroup scheduleRadioGroup;
    public static final int PICKUP_DATE_REQ_CODE = 1453;
    public static final int DELIVERY_DATE_REQ_CODE = 7869;
    public static final int PICKUP_TIME_REQ_CODE = 8549;
    public static final int DELIVERY_TIME_REQ_CODE = 1594;
    Toolbar toolbar;
    SpinnerAdapter hgvCountAdapter;

    public static SubmitReqFragment newInstance() {
        SubmitReqFragment fragment = new SubmitReqFragment();
        /*Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_submit_req, container, false);
        initViews();
        String[] hgvCountArray = getActivity().getResources().getStringArray(R.array.hgv_count);
        List<String> hgvCountList = new ArrayList<>(Arrays.asList(hgvCountArray));
        /*for (int count : hgvCountArray){
            hgvCountList.add(count);
            Log.e("SubmitReqFragment","Count is: "+count);
        }
        hgvCountAdapter = new SpinnerAdapter(getActivity(),hgvCountList,getActivity().getResources().getString(R.string.hgv_count_text));
        hgvCountSpinner.setAdapter(hgvCountAdapter);*/
        hgvCountSpinner.setAdapter(new HintSpinnerAdapter(getActivity(),hgvCountList,getActivity().getResources().getString(R.string.hgv_count_text)));
        return view;
    }

    private void initViews() {
        hgvTypeSpinner = view.findViewById(R.id.hgvTypeSpinner);
        hgvCountSpinner = view.findViewById(R.id.hgvCountSpinner);
        pickupDate = view.findViewById(R.id.pickupDate);
        deliveryDate = view.findViewById(R.id.deliveryDate);
        scheduleRadioGroup = view.findViewById(R.id.radioGroup);
        pickupTime = view.findViewById(R.id.pickupTime);
        deliveryTime = view.findViewById(R.id.deliveryTime);
        submitReqButton = view.findViewById(R.id.submitReqBtn);
        toolbar = view.findViewById(R.id.toolbar);

        pickupDate.setOnClickListener(this);
        deliveryDate.setOnClickListener(this);
        pickupTime.setOnClickListener(this);
        deliveryTime.setOnClickListener(this);
        scheduleRadioGroup.setOnCheckedChangeListener(this);
        submitReqButton.setOnClickListener(this);
        /*hgvTypeSpinner.setOnClickListener(this);
        hgvCountSpinner.setOnClickListener(this);*/

        setupToolBar();
    }

    private void setupToolBar() {
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.left_arrow_64);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId()) {
            case R.id.pickupDate:
                DialogFragment pickupDateFragment = new PickupDatePicker();
                pickupDateFragment.setTargetFragment(SubmitReqFragment.this,PICKUP_DATE_REQ_CODE);
                pickupDateFragment.show(getActivity().getSupportFragmentManager(),getActivity().getResources().getString(R.string.pickup_date_text));
                break;
            case R.id.deliveryDate:
                DialogFragment returnDateFragment = new DeliveryDatePicker();
                returnDateFragment.setTargetFragment(SubmitReqFragment.this,DELIVERY_DATE_REQ_CODE);
                returnDateFragment.show(getActivity().getSupportFragmentManager(),getActivity().getResources().getString(R.string.delivery_date_text));
                break;
            case R.id.pickupTime:
                DialogFragment pickupTimeFragment = new PickupTimePicker();
                pickupTimeFragment.setTargetFragment(SubmitReqFragment.this,PICKUP_TIME_REQ_CODE);
                pickupTimeFragment.show(getActivity().getSupportFragmentManager(),getActivity().getResources().getString(R.string.pickup_time_text));
                break;
            case R.id.deliveryTime:
                DialogFragment deliveryTimeFragment = new DeliveryTimePicker();
                deliveryTimeFragment.setTargetFragment(SubmitReqFragment.this,DELIVERY_TIME_REQ_CODE);
                deliveryTimeFragment.show(getActivity().getSupportFragmentManager(),getActivity().getResources().getString(R.string.delivery_time_text));
                break;
            case R.id.submitReqBtn:
                SuccessOrderFragment.newInstance(getActivity().getSupportFragmentManager());
                break;
            /*case R.id.hgvTypeSpinner:
                break;
            case R.id.hgvCountSpinner:
                int[] hgvCount = getActivity().getResources().getIntArray(R.array.hgv_count);
                ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(getActivity(),R.array.hgv_count,R.layout.support_simple_spinner_dropdown_item);
                hgvCountSpinner
                break;*/
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK)
        {
            switch (requestCode)
            {
                case PICKUP_DATE_REQ_CODE:
                    pickupDate.setText(data.getStringExtra("selectedDate"));
                    pickupDate.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                    break;
                case DELIVERY_DATE_REQ_CODE:
                    deliveryDate.setText(data.getStringExtra("selectedDate"));
                    deliveryDate.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                    break;
                case PICKUP_TIME_REQ_CODE:
                    pickupTime.setText(data.getStringExtra("selectedTime"));
                    break;
                case DELIVERY_TIME_REQ_CODE:
                    deliveryTime.setText(data.getStringExtra("selectedTime"));
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (radioGroup.getCheckedRadioButtonId())
        {
            case R.id.scheduleNowBtn:
                Calendar calendar = Calendar.getInstance();
                pickupDate.setText(new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(calendar.getTime()));
                pickupTime.setText(new SimpleDateFormat("hh:mm a", Locale.ENGLISH).format(calendar.getTime()));
                pickupDate.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                break;
            case R.id.scheduleLaterBtn:
                pickupDate.setText(R.string.pickup_date_text);
                pickupTime.setText(R.string.pickup_time_text);
                pickupDate.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.calendar_icon,0);
                break;
            default:
                break;
        }
    }
}


/*public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }*/