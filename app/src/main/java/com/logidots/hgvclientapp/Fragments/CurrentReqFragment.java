package com.logidots.hgvclientapp.Fragments;

import android.os.Bundle;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.logidots.hgvclientapp.Adapters.RequestAdapter;
import com.logidots.hgvclientapp.R;

public class CurrentReqFragment extends Fragment {
    /*private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;*/
    View view;
    RecyclerView currentReqRV;
    AppCompatTextView emptyCurrentReq;
    RequestAdapter adapter;

    public CurrentReqFragment() {
        // Required empty public constructor
    }

    public static CurrentReqFragment newInstance(String param1, String param2) {
        CurrentReqFragment fragment = new CurrentReqFragment();
        /*Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_current_req, container, false);
        initViews();
        showData();
        return view;
    }

    private void showData() {
        adapter = new RequestAdapter();
        currentReqRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        currentReqRV.setAdapter(adapter);
    }

    private void initViews() {
        currentReqRV = view.findViewById(R.id.currentReqRV);
        emptyCurrentReq = view.findViewById(R.id.emptyCurrentReq);
    }
}


/*public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }*/