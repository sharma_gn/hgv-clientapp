package com.logidots.hgvclientapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    AppCompatTextView labelSignup;
    AppCompatButton loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initViews();
    }

    private void initViews() {
        labelSignup = findViewById(R.id.labelSignUp);
        loginButton = findViewById(R.id.loginBtn);
        labelSignup.setOnClickListener(this);
        loginButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.labelSignUp:
                startActivity(new Intent(LoginActivity.this,SignupActivity.class));
                finish();
                break;
            case R.id.loginBtn:
                startActivity(new Intent(LoginActivity.this,HomeActivity.class));
                finish();
                break;
            default:
                break;
        }
    }
}
