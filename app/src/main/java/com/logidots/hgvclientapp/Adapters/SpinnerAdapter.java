package com.logidots.hgvclientapp.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import java.util.List;

public class SpinnerAdapter extends ArrayAdapter {
    private List objects;
    private String hint;
    private Context context;

    public SpinnerAdapter(Context context, List objects, String hint) {
        super(context, android.R.layout.simple_list_item_1, objects);
        this.objects = objects;
        this.hint = hint;
        this.context = context;
    }

    public boolean hasHint() {
        return hint != null;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView textView;
        if (convertView == null)
        {
            convertView = LayoutInflater.from(context).inflate(android.R.layout.simple_list_item_1,parent,false);
        }
        textView = (TextView) convertView;
        textView.setMaxLines(1);
        if (hint != null && position == 0)
        {
            Log.e("SpinnerAdapter","Position in getDropDownView is: "+position);
            Log.e("SpinnerAdapter","Hint is: "+hint);
            textView.setHint(hint);
            textView.setTextColor(ContextCompat.getColor(context,android.R.color.black));
        }else{
            Log.e("SpinnerAdapter","Position in getDropDownView is: "+position);
            Log.e("SpinnerAdapter","Value is: "+getLabelFor(objects.get(position - 1)));
            textView.setText(getLabelFor(objects.get(position - 1)));
            textView.setTextColor(ContextCompat.getColor(context,android.R.color.black));
        }
        return convertView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView textView;
        if (convertView == null)
        {
            convertView = LayoutInflater.from(context).inflate(android.R.layout.simple_list_item_1,parent,false);
            textView = (TextView) convertView;
            textView.setCompoundDrawables(null,null,ContextCompat.getDrawable(context,android.R.drawable.spinner_background),null);
        }else{
            textView = (TextView) convertView;
        }
        textView.setMaxLines(1);

        if (hint != null && position == 0){
            Log.e("SpinnerAdapter","Position in getView is: "+position);
            Log.e("SpinnerAdapter","Hint is: "+hint);
            textView.setText(hint);
        }else{
            Log.e("SpinnerAdapter","Position in getView is: "+position);
            Log.e("SpinnerAdapter","Value is: "+getLabelFor(objects.get(position - 1)));
            textView.setText(getLabelFor(objects.get(position - 1)));
        }
        return convertView;
    }

    @Override
    public boolean isEnabled(int position) {
        if (hint != null)
            return position != 0 && super.isEnabled(position);

        return super.isEnabled(position);
    }

    private String getLabelFor(Object object) {
        return object.toString();
    }

    @Override
    public int getCount() {
        if (hint != null){
            Log.e("SpinnerAdapter","Count is: "+(super.getCount() + 1));
            return (super.getCount() + 1);
        }else{
            Log.e("SpinnerAdapter","Count is: "+super.getCount());
            return super.getCount();
        }
    }
}
