package com.logidots.hgvclientapp.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.logidots.hgvclientapp.R;

public class DriverListAdapter extends RecyclerView.Adapter<DriverListAdapter.DriverView>{
    @NonNull
    @Override
    public DriverView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.driver_list_item,parent,false);
        return new DriverView(v);
    }

    @Override
    public void onBindViewHolder(@NonNull DriverView holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    class DriverView extends RecyclerView.ViewHolder{
        AppCompatTextView driverName,hgvType;
        AppCompatRatingBar driverRating;
        public DriverView(@NonNull View itemView) {
            super(itemView);
            driverName = itemView.findViewById(R.id.driverNameLabel);
            hgvType = itemView.findViewById(R.id.driverHgvType);
            driverRating = itemView.findViewById(R.id.driverRating);
        }
    }
}
