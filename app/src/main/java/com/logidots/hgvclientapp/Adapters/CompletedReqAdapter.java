package com.logidots.hgvclientapp.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.logidots.hgvclientapp.R;

public class CompletedReqAdapter extends RecyclerView.Adapter<CompletedReqAdapter.CompletedReqView>{
    @NonNull
    @Override
    public CompletedReqView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.completed_req_list_item,parent,false);
        return new CompletedReqView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CompletedReqView holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    class CompletedReqView extends RecyclerView.ViewHolder{
        AppCompatTextView completedReqName,reqPickupTime,reqDeliveryTime,reqDistance;
        public CompletedReqView(@NonNull View itemView) {
            super(itemView);
            completedReqName = itemView.findViewById(R.id.labelCompletedReqName);
            reqPickupTime = itemView.findViewById(R.id.completedReqPickupTime);
            reqDeliveryTime = itemView.findViewById(R.id.completedReqDeliveryTime);
            reqDistance = itemView.findViewById(R.id.completedReqDistance);
        }
    }
}
