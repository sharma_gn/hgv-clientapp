package com.logidots.hgvclientapp.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.logidots.hgvclientapp.R;

public class InvoiceAdapter extends RecyclerView.Adapter<InvoiceAdapter.InvoiceView>{
    @NonNull
    @Override
    public InvoiceView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_invoice,parent,false);
        InvoiceView invoiceView = new InvoiceView(view);
        return invoiceView;
    }

    @Override
    public void onBindViewHolder(@NonNull InvoiceView holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    class InvoiceView extends RecyclerView.ViewHolder{
        AppCompatTextView invoiceID,invoiceReq,invoiceDriver,invoiceTotalPayment;
        public InvoiceView(@NonNull View itemView) {
            super(itemView);
            invoiceID = itemView.findViewById(R.id.invoice_id);
            invoiceReq = itemView.findViewById(R.id.invoice_req);
            invoiceDriver = itemView.findViewById(R.id.invoice_driver);
            invoiceTotalPayment = itemView.findViewById(R.id.invoice_total_payment);
        }
    }
}
