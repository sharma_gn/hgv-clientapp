package com.logidots.hgvclientapp.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.logidots.hgvclientapp.R;

public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.PendingReqView>{

    @NonNull
    @Override
    public PendingReqView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.req_list_item,parent,false);
        return new PendingReqView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PendingReqView holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    class PendingReqView extends RecyclerView.ViewHolder{
        AppCompatTextView reqName,pickupDate,pickupTime;
        public PendingReqView(@NonNull View itemView) {
            super(itemView);
            reqName = itemView.findViewById(R.id.labelReqName);
            pickupDate = itemView.findViewById(R.id.reqPickupDate);
            pickupTime = itemView.findViewById(R.id.reqPickupTime);
        }
    }
}
