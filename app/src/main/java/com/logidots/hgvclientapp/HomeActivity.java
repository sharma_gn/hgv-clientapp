package com.logidots.hgvclientapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.logidots.hgvclientapp.Fragments.CalendarFragment;
import com.logidots.hgvclientapp.Fragments.InvoiceFragment;
import com.logidots.hgvclientapp.Fragments.ProfileFragment;
import com.logidots.hgvclientapp.Fragments.SubmitReqFragment;
import com.logidots.hgvclientapp.Fragments.ViewReqFragment;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener,
        BottomNavigationView.OnNavigationItemSelectedListener {
    CardView cardCalendar,cardSettings,cardSubmitReq,cardViewReq;
    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initViews();
    }

    @Override
    public void onClick(View view) {
        Fragment fragment = null;
        switch (view.getId())
        {
            case R.id.cardSubmitReq:
                fragment = SubmitReqFragment.newInstance();
                break;
            case R.id.cardViewReq:
                fragment = ViewReqFragment.newInstance();
                break;
            case R.id.cardCalendar:
                fragment = CalendarFragment.newInstance();
                break;
            case R.id.cardSettings:
                break;
            default:
                break;
        }
        replaceFragment(fragment);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId())
        {
            case R.id.menu_home:
                break;
            case R.id.menu_invoice:
                Fragment fragment = InvoiceFragment.newInstance();
                replaceFragmentWithSelfTag(fragment);
                break;
            case R.id.menu_profile:
                Fragment profileFragment = ProfileFragment.newInstance();
                replaceFragmentWithSelfTag(profileFragment);
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (getSupportFragmentManager().getBackStackEntryCount()>0)
        {
            String tag = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
            if (tag!=null && !tag.isEmpty() && !tag.equals("null"))
            {
                if (tag.equals(HomeActivity.class.getSimpleName()))
                {
                    bottomNavigationView.getMenu().getItem(0).setChecked(true);
                }else if (tag.equals(InvoiceFragment.class.getSimpleName()))
                {
                    bottomNavigationView.getMenu().getItem(1).setChecked(true);
                }else if (tag.equals(ProfileFragment.class.getSimpleName()))
                {
                    bottomNavigationView.getMenu().getItem(2).setChecked(true);
                }
            }
        }else
            bottomNavigationView.getMenu().getItem(0).setChecked(true);
    }

    private void initViews() {
        cardSubmitReq = findViewById(R.id.cardSubmitReq);
        cardViewReq = findViewById(R.id.cardViewReq);
        cardCalendar = findViewById(R.id.cardCalendar);
        cardSettings = findViewById(R.id.cardSettings);
        bottomNavigationView = findViewById(R.id.bottom_nav_view);

        cardSettings.setOnClickListener(this);
        cardCalendar.setOnClickListener(this);
        cardSubmitReq.setOnClickListener(this);
        cardViewReq.setOnClickListener(this);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
    }

    private void replaceFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main_frame,fragment);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.addToBackStack(HomeActivity.class.getSimpleName());
        transaction.commit();
    }

    private void replaceFragmentWithSelfTag(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main_frame,fragment);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.addToBackStack(fragment.getClass().getSimpleName());
        transaction.commit();
    }
}
